export default {
  max: limit => value => {
      return +value <= +limit || "You should not surpass the limit of " + limit;
  },
  required: value => !!value || "This field is required",
  positiveNum: value => +value >= 0 || "The number must be positive",
  arrayNotEmpty: array => (value, message) => {
      return !!array.length || (message || 'The list must not be empty. Please add something.')
  }
}
