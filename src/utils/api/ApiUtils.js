import axios from 'axios';
import handleError from '@/utils/api/HandleError';
import config from '@/configs';
import { authHeader } from '@/utils/AuthGuard';

let { BASE_URL } = config;

const getHeaders = () => ({
    headers: authHeader(),
})

// HTTP GET Request - Returns Resolved or Rejected Promise
export const get = (path, override = false, override_url) => {
  if (override) BASE_URL = override_url;

  return new Promise((resolve, reject) => {
    axios.get(`${BASE_URL}${path}`, getHeaders())
      .then(response => resolve(response))
      .catch(error => reject(handleError(error)));
  });
};
// HTTP PATCH Request - Returns Resolved or Rejected Promise
export const patch = (path, data, override = false, override_url) => {
  if (override) BASE_URL = override_url;

  return new Promise((resolve, reject) => {
    axios.patch(`${BASE_URL}${path}`, data, getHeaders())
      .then(response => resolve(response))
      .catch(error => reject(handleError(error)));
  });
};
// HTTP POST Request - Returns Resolved or Rejected Promise
export const post = (path, data, override = false, override_url) => {
  if (override) BASE_URL = override_url;

  return new Promise((resolve, reject) => {
    axios.post(`${BASE_URL}${path}`, data, getHeaders())
      .then(response => resolve(response))
      .catch(error => reject(handleError(error)));
  });
};
// HTTP DELETE Request - Returns Resolved or Rejected Promise
export const del = (path, override = false, override_url) => {
  if (override) BASE_URL = override_url;

  return new Promise((resolve, reject) => {
    axios.delete(`${BASE_URL}${path}`, getHeaders())
      .then(response => resolve(response))
      .catch(error => reject(handleError(error)));
  });
};
