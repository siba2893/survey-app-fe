import CustomError from '@/api/models/CustomError';
import Errors from '@/utils/Errors';

// Common Error Handler Function
export default (error) => {
  const { message } = error;
  const { status } = error.response;
  return new CustomError({ message, status, friendlyMessage: Errors[status], type: 'error' });
}
