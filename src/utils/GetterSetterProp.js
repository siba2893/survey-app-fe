export default (mod, key, setFn) => {
  return {
    get() {
      return this.$store.state[mod][key];
    },
    set(v) {
      return this.$store.commit(`${mod}/${setFn}`, {
        key: key,
        value: v
      });
    }
  };
};
