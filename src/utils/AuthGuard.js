export default {
  checkAuthRoutes(router) {
    // get login token from the localStorage
    const token = localStorage.getItem('token');
    router.beforeEach((to, from, next) => {
      if (to.matched.some(record => record.meta.requiresAuth)) {
        if (token === null) {
          next({ path: '/login', params: { nextUrl: to.fullPath } });
        } else {
          next();
        }
      } else next();
    })
  },
  authHeader(){
    // return authorization header with jwt token
    const token = localStorage.getItem('token');

    if (token) {
      return {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      };
    } else {
      return {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      };
    }
  }
}