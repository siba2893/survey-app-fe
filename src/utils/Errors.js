export default {
  404: 'The page you are trying to reach does not exist.',
  401: 'You are currently unauthenticated.',
  403: "You're unauthorized to access this resource.",
  500: 'Oooops something went wrong.'
}
