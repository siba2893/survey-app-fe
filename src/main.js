import Vue from 'vue'
import '@/plugins/vuetify'
import App from '@/App.vue'
import store from '@/stores/index'
import router from '@/router'
import axios from 'axios'

Vue.prototype.$http = axios;

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
