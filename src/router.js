import Vue from "vue";
import Router from "vue-router";
import AuthGuard from '@/utils/AuthGuard';

import TheDashboard from '@/pages/TheDashboard';
import TheMainPage from '@/pages/TheMainPage';
import TheBackOffice from '@/pages/TheBackOffice';
import TheTODOs from '@/pages/TheTodos';
import TheQuestionnaires from '@/pages/TheQuestionnaires';
import TheNotes from '@/pages/TheNotes';
import TheLogin from "@/pages/TheLogin";
import TheRegister from "@/pages/TheRegister";

import The404 from '@/pages/The404';

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: TheMainPage
    },
    {
      path: "/app",
      component: TheBackOffice,
      children: [
        {
          path: "dashboard",
          component: TheDashboard
        },
        {
          path: "todos",
          component: TheTODOs,
          children: []
        },
        {
          path: "questionnaires",
          component: TheQuestionnaires,
          children: []
        },
        {
          path: "notes",
          component: TheNotes,
          children: []
        },
      ],
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      component: TheLogin
    },
    {
      path: '/register',
      component: TheRegister
    },
    { path: "*", component: The404 }
  ]
});

AuthGuard.checkAuthRoutes(router);

export default router;