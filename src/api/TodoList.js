import { get } from '@/utils/api/ApiUtils';
import TodoList from '@/api/models/TodoList';

export default {
  getTodoLists() {
    return new Promise((resolve, reject) => {
      get('/todo-lists').then(response => {
        const data = response.data.map(item => new TodoList(item));
        resolve(data);
      }).catch(reason => {
        reject(reason);
      })
    });
  }
}