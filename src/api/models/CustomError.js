export default class CustomError {
  constructor({ message, status, friendlyMessage, type }) {
    this.message = message;
    this.status = status;
    this.friendlyMessage = friendlyMessage;
    this.type = type;
  }
}
