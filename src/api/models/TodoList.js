export default class TodoList {
  constructor({ id, title, todoItems }) {
    this.id = id;
    this.title = title;
    this.todoItems = todoItems || [];
  }
}
