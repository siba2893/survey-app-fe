export default class TodoItem {
  constructor({ id, title, todoListId }) {
    this.id = id;
    this.title = title;
    this.done = false;
    this.todoListId = todoListId;
  }
}
