import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'
import VuetifySnackbarQueue from 'vuetify-snackbar-queue'

Vue.use(Vuetify, {
  iconfont: 'md',
})

Vue.use(VuetifySnackbarQueue);

