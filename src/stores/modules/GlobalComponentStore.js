export default {
  namespaced: true,

  state: {
    snackBarItems: [],
    fullScreenLoader: {}
  },

  getters: {
    snackBarAlertsCount: (state) => {
      return state.snackBarItems.length
    }
  },

  actions: {
    addSnackBarAlert({ commit, getters }, alertModel) {
      commit('ADD_SNACK_BAR_ALERT', {
        id: getters.snackBarAlertsCount + 1,
        color: alertModel.type,
        message: alertModel.friendlyMessage
      })
    },
    removeSnackBarAlert({ commit }, id) {
      commit('REMOVE_SNACK_BAR_ALERT', id);
    },

    toggleFullScreenLoading({ commit }, payload) {
      commit('UPDATE_FULL_SCREEN_LOADING', payload)
    }
  },

  mutations: {
    ADD_SNACK_BAR_ALERT(state, alert) {
      state.snackBarAlerts.push(alert)
    },

    REMOVE_SNACK_BAR_ALERT(state, id) {
      state.snackBarAlerts = state.snackBarAlerts.filter(item => +item.id !== +id)
    },

    UPDATE_FULL_SCREEN_LOADING(state, payload) {
      state.fullScreenLoader = payload;
    }
  }
}
