import SharedFunc from "@/stores/SharedFunctionalityBetweenStores";

export default {
  namespaced: true,

  state: {
    drawer: false
  },

  getters: {},

  actions: {},

  mutations: {
    ...SharedFunc
  }
}
