import TodoList from '@/api/models/TodoList'

export const actions = {
  fetchTodoLists : ({ commit }) => new Promise((resolve) => {
    commit('SET_TODO_LISTS', []);
    resolve();
  }),
  fetchTodoList : ({ state }, id) => new Promise((resolve) => {
    const todoList = state.todoLists.find(todoList => +todoList.id === +id);
    resolve(todoList);
  }),
  newTodoList : ({ commit, getters, state }) => new Promise((resolve) => {
      // Call Api to create new one
      const newTodoList = new TodoList({
        id: getters.todoListsCount + 1,
        title: state.defaultTodoListName
      });
      commit('ADD_TODO_LIST', newTodoList);
      resolve(newTodoList);
  }),
  deleteTodoList : ({ commit }, todoList) => new Promise((resolve) => {
    commit('DELETE_TODO_LIST', todoList);
    resolve();
  }),
}

export const mutations = {
  SET_TODO_LISTS(state, todoLists) {
    state.todoLists = todoLists;
  },
  ADD_TODO_LIST(state, todoList) {
    state.todoLists.push(todoList);
  },
  DELETE_TODO_LIST(state, todoList) {
    state.todoLists = state.todoLists.filter(_todoList => _todoList.id !== todoList.id);
  },
}
