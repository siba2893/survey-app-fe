import TodoItem from '@/api/models/TodoItem'

export const actions = {
  newTodoItem : ({ commit }, todoItem) => new Promise((resolve) => {
    let newTodoItem = new TodoItem(todoItem);
    commit('ADD_TODO_ITEM', newTodoItem);
    resolve(newTodoItem);
  }),
  deleteTodo : ({ commit }, todoItem) => new Promise((resolve) => {
    commit('DELETE_TODO_ITEM', todoItem);
    resolve();
  }),
  toggleTodoCompleteness : ({ commit }, todoItem) => new Promise((resolve) => {
    commit('TOGGLE_TODO_ITEM', todoItem);
    resolve();
  }),
};

export const mutations = {
  ADD_TODO_ITEM(state, newTodoItem) {
    let todoList = state.todoLists.find(todoList => +todoList.id === +newTodoItem.todoListId);
    newTodoItem.id = todoList.todoItems.length + 1;
    todoList.todoItems.push(newTodoItem);
  },
  DELETE_TODO_ITEM(state, _todoItem) {
    let todoList = state.todoLists.find(todoList => +todoList.id === +_todoItem.todoListId);
    todoList.todoItems = todoList.todoItems.filter(todoItem => +todoItem.id !== +_todoItem.id);
  },
  TOGGLE_TODO_ITEM(state, todoItem) {
    todoItem.done = !todoItem.done;
  },
}
