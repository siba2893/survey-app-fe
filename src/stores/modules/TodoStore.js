import { actions as todoActions, mutations as todoMutations }  from '@/stores/modules/todoItem';
import { actions as todoListActions, mutations as todoListMutations }  from '@/stores/modules/todoList';
import NoteItem from '@/api/models/NoteItem'
import SharedFunc from "@/stores/SharedFunctionalityBetweenStores";
import DateUtils from "@/utils/DateUtils";

const day = DateUtils.currentDay();
const date = new Date().getDate();
const ord = DateUtils.nth(new Date().getDate());

export default {
  namespaced: true,

  state: {
    todoLists: [],
    dialog: false,
    defaultTodoListName: `${ day }, ${ date }${ ord }`
  },

  getters: {
    todoListsCount(state) {
      return state.todoLists.length;
    }
  },

  actions: {
    // ------------------------------------------------- TODO LIST -------------------------------------------------
    ...todoListActions,
    // ------------------------------------------------- TODO ITEM -------------------------------------------------
    ...todoActions,
    // ------------------------------------------------- TODO NOTE -------------------------------------------------
    newNoteItem : ({ commit }, todoItem) => new Promise((resolve) => {
        let newNoteItem = new NoteItem(todoItem);
        commit('ADD_NOTE_ITEM', newNoteItem);
        resolve(newNoteItem);
    }),
    deleteNote({ commit }, noteItem) {
      return new Promise((resolve) => {
        commit('DELETE_NOTE_ITEM', noteItem);
        resolve();
      });
    },
  },

  mutations: {
    // ------------------------------------------------- TODO LIST -------------------------------------------------
    ...todoListMutations,
    // ------------------------------------------------- TODO ITEM -------------------------------------------------
    ...todoMutations,
    ...SharedFunc
  }
}
