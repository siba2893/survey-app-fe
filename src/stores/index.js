import Vue from "vue";
import Vuex from "vuex";
import BaseStore from "@/stores/modules/BaseStore"
import GlobalComponentStore from "@/stores/modules/GlobalComponentStore"
import TODOStore from "@/stores/modules/TodoStore"

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    BaseStore,
    GlobalComponentStore,
    TODOStore
  }
});
