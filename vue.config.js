const path = require("path");
module.exports = { // https://cli.vuejs.org/config/#lintonsave
  lintOnSave: process.env.NODE_ENV !== 'production',
  chainWebpack: config => {
    config.resolve.alias.set('@', path.resolve(__dirname, "src"));
    config.module.rule('eslint').use('eslint-loader').options({
      fix: true
    })
  }
}
